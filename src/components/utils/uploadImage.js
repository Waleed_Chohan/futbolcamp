// import React from "react";
// import { View, Text } from "react-native";
// import { platform } from "os";
// import firebase from "react-native-firebase";

// const Blob = RNFetchBlob.polyfill.Blob;
// const fs = RNFetchBlob.fs;
// window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
// window.Blob = Blob;
// export const uploadImage = (uri, userID) => {
//   mime = "application/octet-stream";
//   return dispatch => {
//     return new Promise((resolve, reject) => {
//       const uploadUri =
//         platform.OS === "ios" ? uri.replace("file://", "") : uri;
//       const sessionId = new Date().getTime();
//       let uploadBlob = null;
//       const imageRef = firebase
//         .storage()
//         .ref("user_profile_images")
//         .child(userID);

//       fs.readFile(uploadUri, "base64")
//         .then(data => {
//           return Blob.build(data, { type: `${mime};BASE64` });
//         })
//         .then(blob => {
//           uploadBlob = blob;
//           return imageRef.put(blob, { contentType: mime });
//         })
//         .then(() => {
//           uploadBlob.close();
//           return imageRef.getDownloadURL();
//         })
//         .catch(error => {
//           reject(error);
//         });
//     });
//   };
// };
