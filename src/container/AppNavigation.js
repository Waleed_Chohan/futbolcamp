import React, { Component } from "react";

import LoginScreen from "./login";
import SignupScreen from "./Signup";
import MenuDashboard from "./dashboard";
import SideMenu from "./sidemenu";
import PlayerScreen from "./palyer";
import DataScreen from "./dataresults";
import ReportScreen from "./reports";
import Mainscreen from "./mainscreen";
import CreateTrainingCamp from "./createtrainingcamp";
import ForgotpasswordScreen from "./forgotpassword";
import AddPlayerScreenOne from "./addplayerscreenone";
import { Text, Image, View } from "react-native";

import {
  createStackNavigator,
  createAppContainer,
  createDrawerNavigator,
  createMaterialTopTabNavigator
} from "react-navigation";

import HumbergerIcon from "../components/utils/HamburgerIcon";
import Swipeable from "react-native-gesture-handler/Swipeable";

const mainScreenStack = createStackNavigator({
  main: {
    screen: Mainscreen,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  createtrainingcamp: {
    screen: CreateTrainingCamp,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  addplayerscreenone: {
    screen: AddPlayerScreenOne,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});

const loginStack = createStackNavigator({
  login: {
    screen: LoginScreen,
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "#1C96BD"
      },
      headerTitleStyle: {
        color: "#fff"
      },
      headerTintColor: "#3a3a3a",
      headerTitle: (
        <Text
          style={{
            color: "#fff",
            fontWeight: "bold",
            fontSize: 20,
            alignSelf: "center",
            padding: 10
          }}
        >
          FutbolCamp
        </Text>
      )
    })
  },
  signup: {
    screen: SignupScreen,
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "#1C96BD"
      },
      headerTitleStyle: {
        color: "#fff"
      },
      headerTintColor: "#3a3a3a",
      headerTitle: (
        <Text
          style={{
            color: "#fff",
            fontWeight: "bold",
            fontSize: 20,
            alignSelf: "center"
          }}
        >
          Registration
        </Text>
      )
    })
  },
  forgotpassword: {
    screen: ForgotpasswordScreen,
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "#1C96BD"
      },
      headerTitleStyle: {
        color: "#fff"
      },
      headerTintColor: "#3a3a3a",
      headerTitle: (
        <Text
          style={{
            color: "#fff",
            fontWeight: "bold",
            fontSize: 20,
            alignSelf: "center"
          }}
        >
          Forgot Password
        </Text>
      )
    })
  }
});

const dashboardStack = createStackNavigator({
  homeScreen: {
    screen: MenuDashboard,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});
const playerStack = createStackNavigator({
  mainScreen: {
    screen: PlayerScreen,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});

const dataresultStack = createStackNavigator({
  mainScreen: {
    screen: DataScreen,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});

const reportStack = createStackNavigator({
  mainScreen: {
    screen: ReportScreen,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});
const tabNavigator = createMaterialTopTabNavigator({
  DashboardScreen: {
    screen: dashboardStack,
    navigationOptions: {
      tabBarLabel: "Home",
      tabBarOptions: {
        style: {
          backgroundColor: "#1C96BD", //<== remove background color
          borderColor: "transparent" // <== remove border
        }
      }

      // tabBarIcon: ({ tintColor }) => (
      //   <Image
      //     size={35}
      //     color={tintColor}
      //   />
      // )
    }
  },
  PlayersScreen: {
    screen: playerStack,
    navigationOptions: {
      tabBarLabel: "Player",
      tabBarOptions: {
        style: {
          backgroundColor: "#1C96BD", //<== remove background color
          borderColor: "transparent" // <== remove border
        }
      },
      tabBarOptions: {
        style: {
          backgroundColor: "#1C96BD", //<== remove background color
          borderColor: "transparent" // <== remove border
        }
      }
      // tabBarIcon: ({ tintColor }) => (
      //   <Image
      //     size={35}
      //     color={tintColor}
      //   />
      // )
    }
  },
  DataScreen: {
    screen: dataresultStack,
    navigationOptions: {
      tabBarLabel: "Data",
      tabBarOptions: {
        style: {
          backgroundColor: "#1C96BD", //<== remove background color
          borderColor: "transparent" // <== remove border
        }
      }
      // tabBarIcon: ({ tintColor }) => (
      //   <Image
      //     size={35}
      //     color={tintColor}
      //   />
      // )
    }
  },
  ReportScreen: {
    screen: reportStack,
    navigationOptions: {
      tabBarLabel: "Reports",
      tabBarOptions: {
        style: {
          backgroundColor: "#1C96BD", //<== remove background color
          borderColor: "transparent" // <== remove border
        }
      }
      // tabBarIcon: ({ tintColor }) => (
      //   <Image
      //     size={35}
      //     color={tintColor}
      //   />
      // )
    }
  }
});
const Drawer = createDrawerNavigator(
  {
    Tabs: { screen: tabNavigator }
  },
  {
    contentComponent: SideMenu,
    contentOptions: {
      activeTintColor: "#000"
    },
    drawerWidth: 300,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: true
    })
  }
);
const primaryStack = createStackNavigator(
  {
    login: {
      screen: loginStack,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    menudashboard: {
      screen: Drawer,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: "#1C96BD"
        },
        headerTitleStyle: {
          color: "#fff"
        },
        headerTintColor: "#3a3a3a",
        headerTitle: (
          <Text
            style={{
              color: "#fff",
              fontWeight: "bold",
              fontSize: 25,
              alignSelf: "center"
            }}
          >
            Home Page
          </Text>
        ),
        headerLeft: <HumbergerIcon navigationProps={navigation} />
        //headerLeft: null
      })
    },

    playeraddScreen: {
      screen: mainScreenStack,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    }
  },
  {
    initialRouteName: "login"
  }
);

const App = createAppContainer(primaryStack);

export default App;
