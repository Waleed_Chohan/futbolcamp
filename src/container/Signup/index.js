import React, { Component } from "react";
import {
  Text,
  View,
  Platform,
  TextInput,
  TouchableHighlight,
  Alert,
  Image,
  TouchableOpacity
} from "react-native";
import Styles from "./styles";
import DatePicker from "react-native-datepicker";
import firebase, { auth } from "react-native-firebase";
import { platform } from "os";
import Loader from "../../components/utils/loader";
var ImagePicker = require("react-native-image-picker");
var options = {
  title: "Select Image",
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};
export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      password: "",
      email: "",
      dateofbirth: "",
      loading: false,
      errorMessage: "",
      imageUrl: "",
      avatarSource: require("../../components/assets/profileimage.png")
    };
    this.signupUser = this.signupUser.bind(this);
  }

  editphotoclick = () => {
    ImagePicker.showImagePicker(options, response => {
      this.setState({ loading: true });

      if (response.didCancel) {
        this.setState({ loading: false });
      } else if (response.error) {
        this.setState({ loading: false });
      } else if (response.customButton) {
        this.setState({ loading: false });
      } else {
        const source = { uri: response.uri };
        this.setState({
          avatarSource: source,
          loading: false
        });
      }
    });
  };
  signupUser = async () => {
    this.setState({
      loading: true
    });

    await firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {
        const imageRef = firebase
          .storage()
          .ref("user_profile_images")
          .child(firebase.auth().currentUser.uid);

        imageRef.put(this.state.avatarSource.uri).then(() => {
          imageRef.getDownloadURL().then(downloadURL => {
            this.setState({
              imageUrl: downloadURL
            });

            firebase
              .database()
              .ref("users/" + firebase.auth().currentUser.uid)
              .set(
                {
                  firstName: this.state.firstname,
                  lastName: this.state.lastname,
                  email: this.state.email,
                  dateofbirth: this.state.dateofbirth,
                  imageUrl: this.state.imageUrl
                },
                error => {
                  if (error) {
                    Alert.alert(error.message);
                  } else {
                    this.setState({
                      loading: false
                    });
                    Alert.alert("User Registered successfully");
                    this.props.navigation.navigate("DashboardScreen");
                  }
                }
              );
          });
        });
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({
          loading: false
        });
      });
  };

  render() {
    return (
      <View style={Styles.container}>
        <Loader loading={this.state.loading} />
        <View style={Styles.inputContainer}>
          <Image
            source={this.state.avatarSource}
            style={{
              width: 100,
              height: 100,
              borderRadius: 100 / 2,
              padding: 10
            }}
          />
          <TouchableOpacity
            style={Styles.forgotPassword}
            onPress={this.editphotoclick}
          >
            <Text style={{ color: "blue", fontSize: 13 }}>Edit Photo</Text>
          </TouchableOpacity>
          <TextInput
            style={Styles.inputs}
            placeholder="First Name"
            textAlign={"left"}
            underlineColorAndroid="transparent"
            value={this.state.firstname}
            onChangeText={firstname => this.setState({ firstname })}
          />
          <TextInput
            style={Styles.inputs}
            placeholder="Last Name"
            textAlign={"left"}
            underlineColorAndroid="transparent"
            value={this.state.username}
            onChangeText={lastname => this.setState({ lastname })}
          />

          <TextInput
            style={Styles.inputs}
            placeholder="Enter Email"
            textAlign={"left"}
            value={this.state.email}
            underlineColorAndroid="transparent"
            onChangeText={email => this.setState({ email })}
          />

          <TextInput
            style={Styles.inputs}
            placeholder="Enter Password"
            secureTextEntry={true}
            textAlign={"left"}
            underlineColorAndroid="transparent"
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
          />
          <DatePicker
            style={Styles.dateinput}
            date={this.state.dateofbirth}
            mode="date"
            placeholder="Enter Date Of Birth"
            placeholderTextColor="#eeeeee"
            format="MM-DD-YYYY"
            maxDate={new Date()}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                left: 0,
                top: 4,
                marginLeft: 0
              },
              dateInput: {
                borderWidth: 0,
                alignItems: "flex-start",
                fontSize: 18
              },
              dateText: {
                fontSize: 18,
                color: "#000000",
                fontFamily: "bold"
              }
            }}
            onDateChange={date => {
              this.setState({ dateofbirth: date });
            }}
          />
          <View style={{ alignSelf: "center" }}>
            <TouchableHighlight
              style={[Styles.buttonContainer, Styles.submitButton]}
              onPress={this.signupUser}
            >
              <Text style={Styles.submitText}>SUBMIT</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
}
