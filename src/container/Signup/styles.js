import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: "#fff",
    flex: 1
  },
  inputContainer: {
    backgroundColor: "transparent",
    marginLeft: 10,
    marginRight: 10,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  inputs: {
    height: 45,
    borderBottomColor: "#000",
    borderBottomWidth: 1,
    backgroundColor: "#fff",
    marginBottom: 10,
    fontSize: 18,
    textAlign: "left",
    width: "100%"
  },
  dateinput: {
    height: 45,
    borderBottomColor: "#000",
    borderBottomWidth: 1,
    backgroundColor: "#fff",
    marginBottom: 10,
    width: "100%"
  },
  buttonContainer: {
    height: 38,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 15,
    width: 150,
    borderRadius: 30
  },
  submitButton: {
    borderColor: "grey",
    backgroundColor: "#000"
  },
  submitText: {
    color: "white",
    fontSize: 17,
    fontWeight: "bold",
    textAlign: "center"
  },
  forgotPassword: {
    height: 45,
    borderRadius: 30,
    alignItems: "flex-end",
    marginTop: 10
  }
});

export default styles;
