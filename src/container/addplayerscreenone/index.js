import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
  TextInput
} from "react-native";
import styles from "./styles";

export default class AddplayerScreenOne extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarSource: require("../../components/assets/profileimage.png"),
      firstname: "",
      lastname: ""
    };
  }

  nextButtonClick = () => {
    this.props.navigation.navigate("addplayerscreenone");
  };

  render() {
    return (
      <ImageBackground
        source={require("../../components/assets/background.png")}
        style={styles.maincontainer}
      >
        <View style={styles.container}>
          <Image
            source={this.state.avatarSource}
            style={styles.roundImageStyle}
          />

          <Text style={styles.labels}>
            {"Hi" + " " + this.state.firstname + " " + this.state.lastname}
          </Text>
          <View
            style={{
              borderBottomColor: "#000",
              width: "50%",
              height: 1,
              borderBottomWidth: 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          />
        </View>

        <View style={styles.belowcontainer}>
          <Text style={styles.forgotPassword}>
            Let's create your players now
          </Text>
          <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => this.nextButtonClick()}
          >
            <Text style={styles.loginText}>ADD PLAYER</Text>
          </TouchableOpacity>
          <Text style={styles.forgotPassword}>
            Or you can create them later from training camp options
          </Text>
          <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => this.nextButtonClick()}
          >
            <Text style={styles.loginText}>GO TO TRAINING CAMP</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}
