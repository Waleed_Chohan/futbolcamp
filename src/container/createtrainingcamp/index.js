import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
  TextInput
} from "react-native";
import styles from "./styles";
export default class CreateTrainingCampScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarSource: require("../../components/assets/profileimage.png"),
      firstname: "",
      lastname: ""
    };
  }

  nextButtonClick = () => {
    this.props.navigation.navigate("addplayerscreenone");
  };

  render() {
    return (
      <ImageBackground
        source={require("../../components/assets/background.png")}
        style={styles.maincontainer}
      >
        <View style={styles.container}>
          <Image
            source={this.state.avatarSource}
            style={styles.roundImageStyle}
          />

          <Text style={styles.labels}>
            {"Hi" + " " + this.state.firstname + " " + this.state.lastname}
          </Text>
          <View
            style={{
              borderBottomColor: "#000",
              width: "50%",
              height: 1,
              borderBottomWidth: 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          />
        </View>

        <View style={styles.belowcontainer}>
          <Text style={styles.forgotPassword}>
            We will create your own Futbol Training camp
          </Text>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputs}
              placeholder="Camp Name"
              underlineColorAndroid="transparent"
              textAlign={"left"}
              placeholderTextColor="grey"
              value={this.state.email}
              onChangeText={email => this.setState({ email })}
            />
          </View>
          <Text style={styles.forgotPassword}>
            You are Step closer to get everything ready!
          </Text>
          <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => this.nextButtonClick()}
          >
            <Text style={styles.loginText}>NEXT</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}
