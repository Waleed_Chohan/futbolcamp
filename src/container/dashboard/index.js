import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import styles from "./styles";
export default class DashboardScreen extends Component {
  loginPress = () => {};

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.cardcontainer}>
          <Text style={{ marginTop: 10 }}>
            You Have 0 Players in Your Futbol Training Camp
          </Text>

          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => this.loginPress()}
          >
            <Text style={styles.loginText}>PLAYER LIST</Text>
          </TouchableOpacity>

          <View
            style={{
              borderBottomColor: "black",
              borderBottomWidth: 1
            }}
          />
        </View>

        <View style={styles.cardcontainer}>
          <Text style={{ marginTop: 10 }}>
            Input Data Results To Keep Your Players On Track
          </Text>

          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => this.loginPress()}
          >
            <Text style={styles.loginText}>DATA RESULTS</Text>
          </TouchableOpacity>

          <View
            style={{
              borderBottomColor: "black",
              borderBottomWidth: 1
            }}
          />
        </View>

        <View style={styles.cardcontainer}>
          <Text style={{ marginTop: 10 }}>
            Check Your Futbol Training Camp Progress
          </Text>

          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => this.loginPress()}
          >
            <Text style={styles.loginText}>REPORTS</Text>
          </TouchableOpacity>

          <View
            style={{
              borderBottomColor: "black",
              borderBottomWidth: 1
            }}
          />
        </View>
      </View>
    );
  }
}
