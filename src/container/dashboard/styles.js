import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Platform,
  DeviceInfo,
  TextInput
} from "react-native";

module.exports = StyleSheet.create({
  buttonContainer: {
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: 120,
    borderRadius: 4,
    backgroundColor: "#6DD92E"
  },
  container: {
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: "#fff",
    flexDirection: "column"
  },

  cardcontainer: {
    flex: 1,
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 4,
    borderWidth: 0.5,
    margin: 5,
    borderColor: "#000"
  }
});
