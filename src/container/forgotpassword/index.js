import React, { Component } from "react";
import {
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Alert
} from "react-native";
import styles from "./styles";
import { connect } from "react-redux";
import firebase from "react-native-firebase";
import { changePassword } from "../../store/actions/index";
import Loader from "../../components/utils/loader";
import Validator from "../../components/utils/validator";
class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      email: ""
    };
  }

  resetPassword = () => {
    if (Validator.isValidFieldValue(this.state.email)) {
      if (Validator.validateEmail(this.state.email)) {
        this.props.changePassword(this.state.email);
      } else {
        Alert.alert("Please provide valid email address.");
      }
    } else {
      Alert.alert("Please provide account deatils.");
    }
  };

  showLoader() {
    if (this.props.loading) {
      return <Loader loading={this.state.loading} />;
    }
  }
  renderMessage() {
    if (this.props.passwordChangeMessage) {
      return (
        <View
          style={{
            backgroundColor: "white",
            alignItems: "center",
            margin: 10
          }}
        >
          <Text style={styles.errorTextStyle}>
            {this.props.passwordChangeMessage}
          </Text>
        </View>
      );
    }
  }

  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.fullSize}>
        <ScrollView
          contentContainerStyle={styles.forgot_password_container}
          keyboardShouldPersistTaps="never"
          scrollEnabled={false}
        >
          <View style={styles.forgot_password_form_container}>
            {this.showLoader()}

            <Text>{this.state.errorMessage}</Text>
            <TextInput
              style={styles.forgot_password_input}
              onChangeText={email => this.setState({ email })}
              value={this.state.email}
              placeholder="EMAIL ADDRESS"
              autoCapitalize="none"
              keyboardType="email-address"
              //onFocus={() => this.setState({ username: "" })}
              underlineColorAndroid="#fff"
            />
            {this.renderMessage()}
          </View>
          <View style={styles.forgot_password_actions_container}>
            <TouchableOpacity
              onPress={() => this.resetPassword()}
              style={styles.forgot_password_button}
            >
              <Text style={styles.forgot_password_button_text}>RECOVER</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  return {
    passwordChangeMessage: state.auth.passwordChangeMessage,
    loading: state.auth.loading
  };
};

export default connect(
  mapStateToProps,
  { changePassword }
)(ForgotPassword);
