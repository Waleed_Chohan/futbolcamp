const React = require("react-native");
const { Dimensions, StyleSheet } = React;

module.exports = StyleSheet.create({
  fullSize: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height
  },
  forgot_password_container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center"
    // justifyContent: 'space-between',
    // marginBottom: -10,
  },
  forgot_password_form_container: {
    // flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
    marginTop: 130
  },
  forgot_password_input: {
    width: 300,
    height: 40,
    borderColor: "gray",
    borderBottomWidth: 1,
    textAlign: "left",
    fontSize: 15,
    marginBottom: 20
  },
  forgot_password_actions_container: {
    alignItems: "center",
    justifyContent: "flex-end"
  },
  login_button: {
    backgroundColor: "#fff",
    color: "lightgrey",
    width: 200,
    margin: 10,
    height: 20,
    fontSize: 10,
    textAlign: "center",
    textAlignVertical: "center"
  },
  forgot_password_button: {
    borderColor: "grey",
    backgroundColor: "#000",
    borderRadius: 30,
    borderWidth: 1,
    width: 180,
    height: 40,
    justifyContent: "center"
  },
  forgot_password_button_text: {
    color: "#fff",
    fontSize: 17,
    textAlign: "center",
    alignContent: "center",
    fontWeight: "bold"
  }
});
