import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  Alert
} from "react-native";
import styles from "./styles";
import firebase from "react-native-firebase";
import { connect } from "react-redux";
import { loginUser } from "../../store/actions/index";
import Loader from "../../components/utils/loader";
import Validator from "../../components/utils/validator";
import { GoogleSignin, GoogleSigninButton } from "react-native-google-signin";
GoogleSignin.configure();

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  // getDrivedStateFromProps(nextProps) {
  //   if (nextProps.UserID !== this.props.UserID) {
  //     this.props.navigation.navigate("homeScreen");
  //   }
  // }

  componentWillReceiveProps(nextProps) {
    if (nextProps.UserID !== this.props.UserID) {
      this.props.navigation.navigate("homeScreen");
    }
  }

  componentDidMount() {}

  componentWillMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.props.navigation.navigate("homeScreen");
      }
    });
  }

  loginPress = async () => {
    if (
      Validator.isValidFieldValue(this.state.email) &&
      Validator.isValidFieldValue(this.state.password)
    ) {
      if (Validator.validateEmail(this.state.email)) {
        await this.props.loginUser(this.state.email, this.state.password);
      } else {
        Alert.alert("Please provide valid email address.");
      }
    } else {
      Alert.alert("Please provide account deatils.");
    }
  };

  signinWithGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      Alert.alert(
        userInfo.user.name +
          "\n" +
          userInfo.user.email +
          "\n" +
          userInfo.user.givenName +
          "\n" +
          userInfo.user.familyName
      );
      // Alert.alert("Sign in");
    } catch (error) {
      // if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      //   // user cancelled the login flow
      // } else if (error.code === statusCodes.IN_PROGRESS) {
      //   // operation (f.e. sign in) is in progress already
      // } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      //   // play services not available or outdated
      // } else {
      //   // some other error happened
    }
  };

  forgotPasswordClicked = () => {
    // this.props.navigation.navigate("forgotpassword");

    //this.props.navigation.navigate("playeraddScreen");

    //this.props.navigation.navigate("createtrainingcamp");

    this.props.navigation.navigate("homeScreen");

    // Alert.alert("INISDE CLICK");
  };
  navigateToMainPage() {
    if (this.props.UserID) {
      //Alert.alert(this.props.UserID);
      this.props.navigation.navigate("homeScreen");
      this.setState({
        loading: false
      });
    }
  }

  renderError() {
    if (this.props.message) {
      return (
        <View
          style={{
            backgroundColor: "white",
            alignItems: "center",
            margin: 10
          }}
        >
          <Text style={styles.errorTextStyle}>{this.props.message}</Text>
        </View>
      );
    }
  }

  showLoader() {
    if (this.props.loading) {
      return <Loader loading={this.state.loading} />;
    }
  }
  render() {
    return (
      <View style={styles.container}>
        {this.showLoader()}
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="Email Address"
            keyboardType="email-address"
            underlineColorAndroid="transparent"
            textAlign={"left"}
            placeholderTextColor="grey"
            value={this.state.email}
            onChangeText={email => this.setState({ email })}
          />
          <View
            style={{
              borderBottomColor: "grey",
              width: "100%",
              height: 1,
              borderBottomWidth: 1
            }}
          />
          <TextInput
            style={styles.inputs}
            placeholder="Password"
            secureTextEntry={true}
            textAlign={"left"}
            placeholderTextColor="grey"
            underlineColorAndroid="transparent"
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
          />
        </View>
        {this.renderError()}

        <TouchableOpacity
          style={styles.forgotPassword}
          onPress={this.forgotPasswordClicked}
        >
          <Text style={{ color: "grey", fontWeight: "bold", fontSize: 15 }}>
            Forget Password?
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.buttonContainer, styles.loginButton]}
          onPress={() => this.loginPress()}
        >
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>

        <View
          style={{
            flexDirection: "column",
            backgroundColor: "transparent",
            height: "35%",
            width: "100%",
            alignItems: "center",
            padding: 20
          }}
        >
          <View
            style={{
              borderBottomColor: "#000",
              width: "20%",
              height: 1,
              borderBottomWidth: 1,
              alignItems: "center"
            }}
          />
          <GoogleSigninButton
            style={{ width: 192, height: 48 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={this.signinWithGoogle}
          />
          <Text
            style={{
              alignItems: "center",
              color: "grey",
              margin: 20,
              fontWeight: "bold"
            }}
          >
            Don't have an Account?
          </Text>
          <TouchableHighlight
            style={styles.signUp}
            onPress={() => this.props.navigation.navigate("signup")}
          >
            <Text
              style={{
                color: "#fff",
                fontSize: 17,
                textAlign: "center",
                alignContent: "center",
                fontWeight: "bold"
              }}
            >
              SIGNUP
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    // email,
    // password,
    message: state.auth.message,
    UserID: state.auth.UserID,
    loading: state.auth.loading
  };
};
GoogleSignin.configure({
  scopes: ["https://www.googleapis.com/auth/drive.readonly"], // what API you want to access on behalf of the user, default is email and profile
  webClientId:
    "791468166938-iqpih7e4v3pn9ck0eea0u81v7iqbm55d.apps.googleusercontent.com", // client ID of type WEB for your server (needed to verify user ID and offline access)
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  hostedDomain: "", // specifies a hosted domain restriction
  loginHint: "", // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
  forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
  accountName: "", // [Android] specifies an account name on the device that should be used
  iosClientId: "" // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});
export default connect(
  mapStateToProps,
  { loginUser }
)(LoginScreen);
