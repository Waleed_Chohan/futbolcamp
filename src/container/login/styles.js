const React = require("react-native");
const { StyleSheet } = React;

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    flexDirection: "column"
  },
  inputContainer: {
    borderColor: "grey",
    backgroundColor: "#fff",
    borderRadius: 10,
    borderWidth: 1,
    width: 300,
    height: 120,
    marginTop: 100,
    flexDirection: "column"
  },
  inputs: {
    height: 45,
    marginLeft: 5,
    flex: 1,
    fontSize: 18,
    textAlign: "left"
  },
  forgotPassword: {
    height: 45,
    borderRadius: 30,
    alignItems: "flex-end",
    marginTop: 10
  },
  buttonContainer: {
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: 180,
    borderRadius: 30
  },
  loginButton: {
    backgroundColor: "#6DD92E"
  },
  signUp: {
    borderColor: "grey",
    backgroundColor: "#000",
    borderRadius: 30,
    borderWidth: 1,
    width: 180,
    height: 40,
    justifyContent: "center"
  }
});
