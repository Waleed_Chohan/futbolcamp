import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageBackground
} from "react-native";
import styles from "./styles";
export default class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarSource: require("../../components/assets/profileimage.png"),
      firstname: "",
      lastname: ""
    };
  }

  loginPress = () => {
    this.props.navigation.navigate("createtrainingcamp");
  };

  render() {
    return (
      <ImageBackground
        source={require("../../components/assets/background.png")}
        style={styles.maincontainer}
      >
        <View style={styles.container}>
          <Image
            source={this.state.avatarSource}
            style={styles.roundImageStyle}
          />

          <Text style={styles.labels}>
            {"Welcome" + " " + this.state.firstname + " " + this.state.lastname}
          </Text>
          <View
            style={{
              borderBottomColor: "#000",
              width: "50%",
              height: 1,
              borderBottomWidth: 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          />
        </View>

        <View style={styles.belowcontainer}>
          <Text style={styles.forgotPassword}> Let's Start </Text>

          <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => this.loginPress()}
          >
            <Text style={styles.loginText}>
              Create a new Futbol training Camp
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}
