const React = require("react-native");
const { StyleSheet } = React;

module.exports = StyleSheet.create({
  maincontainer: {
    flex: 1
  },
  container: {
    height: "40%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  belowcontainer: {
    flex: 1,
    alignItems: "center",
    flexDirection: "column"
  },
  roundImageStyle: {
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    padding: 10
  },
  inputContainer: {
    borderColor: "grey",
    backgroundColor: "#fff",
    borderRadius: 10,
    borderWidth: 1,
    width: 300,
    height: 120,
    marginTop: 100,
    flexDirection: "column"
  },
  labels: {
    height: 45,
    marginLeft: 5,
    fontSize: 18,
    textAlign: "left"
  },
  forgotPassword: {
    height: 45,
    alignItems: "flex-end",
    marginTop: 10
  },
  buttonContainer: {
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: 250,
    borderRadius: 5,
    marginTop: 100
  },
  loginButton: {
    backgroundColor: "#6DD92E"
  },
  signUp: {
    borderColor: "grey",
    backgroundColor: "#000",
    borderRadius: 30,
    borderWidth: 1,
    width: 180,
    height: 40,
    justifyContent: "center"
  }
});
