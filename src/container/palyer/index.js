import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView
} from "react-native";

import RadioForm from "react-native-simple-radio-button";
import styles from "./styles";
import DatePicker from "react-native-datepicker";

import CountryPicker from "react-native-country-picker-modal";

export default class PlayerScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dateofbirth: "",
      avatarSource: require("../../components/assets/profileimage.png"),
      cca2: "PK",
      country: "Click Flag to Select"
    };
  }

  render() {
    var radio_props = [
      { label: "Male", value: "Male" },
      { label: "Female", value: "Female" }
    ];
    return (
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.textlabel}>New Player</Text>

          <View style={styles.rowcontainer}>
            <Text style={styles.texttittle}>Name</Text>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                underlineColorAndroid="transparent"
                textAlign={"left"}
                placeholderTextColor="grey"
                value=""
              />
            </View>
          </View>

          <View style={styles.rowcontainer}>
            <Text style={styles.texttittle}>Last Name</Text>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                underlineColorAndroid="transparent"
                textAlign={"left"}
                placeholderTextColor="grey"
                value=""
              />
            </View>
          </View>

          <View style={styles.rowcontainer}>
            <Text style={styles.texttittle}>Gender</Text>
            <View style={styles.radioContainer}>
              <RadioForm
                radio_props={radio_props}
                initial={0}
                formHorizontal={true}
              />
            </View>
          </View>

          <View style={styles.rowcontainer}>
            <Text style={styles.texttittle}>Date of Birth</Text>
            <View style={styles.inputContainer}>
              <DatePicker
                style={styles.dateinput}
                date={this.state.dateofbirth}
                mode="date"
                placeholderTextColor="#eeeeee"
                format="MM-DD-YYYY"
                maxDate={new Date()}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  },
                  dateInput: {
                    borderWidth: 0,
                    alignItems: "flex-start",
                    fontSize: 18
                  },
                  dateText: {
                    fontSize: 18,
                    color: "#000000",
                    fontFamily: "bold"
                  }
                }}
                onDateChange={date => {
                  //this.setState({ dateofbirth: date });
                }}
              />
            </View>
          </View>

          <View style={styles.rowcontainer}>
            <Text style={styles.texttittle}>Place of Birth</Text>
            <View style={styles.inputContainer}>
              <View style={styles.rowcontainer}>
                <CountryPicker
                  onChange={value => {
                    this.setState({ country: value.name, cca2: value.cca2 });
                  }}
                  cca2={this.state.cca2}
                  translation="eng"
                />
                {this.state.country && <Text>{this.state.country}</Text>}
              </View>
            </View>
          </View>
          <View style={styles.rowcontainer}>
            <Text style={styles.texttittle}>Age</Text>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                underlineColorAndroid="transparent"
                textAlign={"left"}
                placeholderTextColor="grey"
                value=""
              />
            </View>
          </View>

          <View style={styles.container}>
            <TouchableOpacity
              style={[styles.buttonContainer, styles.loginButton]}
              onPress={() => this.loginPress()}
            >
              <Text style={styles.loginText}>ADD PICTURE ( optional )</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.container}>
            <TouchableOpacity
              style={[styles.buttonContainer, styles.loginButton]}
              onPress={() => this.loginPress()}
            >
              <Text style={styles.loginText}>SAVE PLAYER</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.container} />

          <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => this.loginPress()}
          >
            <Text style={styles.loginText}>SAVE AND ADD PLAYER</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => this.loginPress()}
          >
            <Text style={styles.loginText}>GO TO TRAINING CAMP</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
