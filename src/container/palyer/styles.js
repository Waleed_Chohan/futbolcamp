import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Platform,
  DeviceInfo,
  TextInput
} from "react-native";

module.exports = StyleSheet.create({
  buttonContainer: {
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: 300,
    borderRadius: 4,
    marginBottom: 15,
    backgroundColor: "#6DD92E"
  },
  rowButton: {
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: 150,
    borderRadius: 4,
    marginBottom: 15,
    backgroundColor: "#6DD92E"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#fff",
    alignItems: "center",
    flexDirection: "column"
  },
  rowButtonContainer: {
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: "#fff",
    alignItems: "center",
    flexDirection: "row"
  },

  rowcontainer: {
    flex: 1,
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 4,
    padding: 10,
    flexDirection: "row"
  },
  textlabel: {
    height: 45,
    borderRadius: 30,
    marginTop: 15,
    alignItems: "flex-end",
    textAlign: "center"
  },

  texttittle: {
    alignItems: "center",
    flexDirection: "column",
    width: "30%"
  },

  flagtittle: {
    alignItems: "center",
    flexDirection: "column",

    height: 45,
    borderRadius: 30,
    marginTop: 15,
    alignItems: "flex-end",
    textAlign: "center"
  },
  inputContainer: {
    borderColor: "grey",
    backgroundColor: "#fff",
    borderRadius: 10,
    borderWidth: 1,
    width: "70%",
    height: 50,
    flexDirection: "column"
  },
  radioContainer: {
    backgroundColor: "#fff",
    width: "70%",
    height: 50,
    flexDirection: "column"
  },
  dateinput: {
    height: 45,
    width: "100%"
  },
  contentContainer: {
    paddingVertical: 20
  }
});
