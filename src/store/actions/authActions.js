import { authActions } from "../types/types";
import firebase from "react-native-firebase";
import { Alert } from "react-native";
export const loginUser = (email, password) => dispatch => {
  dispatch({ type: authActions.LOGIN_USER });
  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(() => loginUserSucess(dispatch, firebase.auth().currentUser.uid))
    .catch(err => loginUserFail(dispatch, err.message));
};

export const loginUserSucess = (dispatch, uid) => {
  dispatch({
    type: authActions.SIGN_IN_SUCCESS,
    payload: firebase.auth().currentUser.uid
  });
};

export const loginUserFail = (dispatch, message) => {
  dispatch({
    type: authActions.SIGN_IN_FAILED,
    payload: message
  });
};

export const changePassword = email => dispatch => {
  dispatch({
    type: authActions.PASSWORD_CHANGE
  });
  firebase
    .auth()
    .sendPasswordResetEmail(email)
    .then(() => {
      passwordChangeSuccess(dispatch);
    })
    .catch(error => {
      passwordChangeFail(dispatch, error.message);
    });
};

export const passwordChangeSuccess = dispatch => {
  dispatch({
    type: authActions.PASSWORD_CHANGE_SUCCESS
  });
};

export const passwordChangeFail = (dispatch, message) => {
  dispatch({
    type: authActions.PASSWORD_CHANGE_FAIL,
    payload: message
  });
};
