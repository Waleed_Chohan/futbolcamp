import { authActions } from "../types/types";
import firebase from "react-native-firebase";
import { Alert } from "react-native";
import { NavigationActions } from "react-navigation";

const initialState = {
  message: "",
  UserID: "",
  loading: false,
  passwordChangeMessage: ""
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case authActions.SIGN_IN_SUCCESS:
      return { ...state, UserID: action.payload, loading: false };

    case authActions.SIGN_IN_FAILED:
      return { ...state, message: action.payload, loading: false };

    case authActions.LOGIN_USER:
      return { ...state, loading: true, message: "" };

    case authActions.PASSWORD_CHANGE:
      return { ...state, loading: true, passwordChangeMessage: "" };

    case authActions.PASSWORD_CHANGE_SUCCESS:
      return {
        ...state,
        loading: false,
        passwordChangeMessage:
          "PLEASE CHECK YOUR EMAIL FOR FURTHER INSTRUCTIONS"
      };

    case authActions.PASSWORD_CHANGE_FAIL:
      return {
        ...state,
        loading: false,
        passwordChangeMessage: action.payload
      };

    default:
      return state;
  }
};

export default reducer;
